<?php

/**
 * @file
 * Command to generate and analyze the dependency graph of installed modules.
 */

// Module status constants
define('DEPENDENCY_ANALYSIS_UNINSTALLED', -1);
define('DEPENDENCY_ANALYSIS_DISABLED', 0);
define('DEPENDENCY_ANALYSIS_ENABLED', 1);

/**
 * Implements hook_drush_command().
 */
function dependency_analysis_drush_command() {
  $items = array();
  $items['analyze-modules'] = array(
    'description' => dt('Analyze dependencies of available modules.'),
    'aliases' => array('am'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function dependency_analysis_drush_help($section) {
  switch ($section) {
    case 'drush:analyze-modules':
      return dt('Analyze module dependencies');

    case 'meta:dependency_analysis:title':
      return dt('Tool for analyzing Drupal module dependencies');

  }
}

/**
 * Implements drush_hook_COMMANDNAME() for analyze-modules.
 *
 * Creates a dot-language representation of the dependency graph of available
 * modules.  Analyzes the graph for circular dependencies and redundant
 * subdependencies.
 */
function drush_dependency_analysis_analyze_modules() {
  list($graph_edges, $module_statuses) = _dependency_analysis_get_graph_edges();
  _dependency_analysis_analyze($graph_edges, $module_statuses);
  _dependency_analysis_generate_dot($graph_edges, $module_statuses);
}

/**
 * Generates edge data for the module dependency graph.  Returns an array
 * where each key is a module name, and the value is an array of immediate
 * dependencies.
 */
function _dependency_analysis_get_graph_edges() {
  $dependencies = array();
  $statuses = array();
  $module_data = system_rebuild_module_data();
  foreach ($module_data as $name => $data) {
    // skip hidden modules
    if (!empty($data->info['hidden'])) {
      continue;
    }
    // determine module status
    if ($data->schema_version == -1) {
      $statuses[$name] = DEPENDENCY_ANALYSIS_UNINSTALLED;
    }
    elseif ($data->status == 1) {
      $statuses[$name] = DEPENDENCY_ANALYSIS_ENABLED;
    }
    else {
      $statuses[$name] = DEPENDENCY_ANALYSIS_DISABLED;
    }
    // add dependencies
    $dependencies[$name] = $data->info['dependencies'];
    // sanitize names: Drupal adds version requirements to the names
    foreach ($dependencies[$name] as &$dependency) {
      $dependency = _dependency_analysis_sanitize_module_name($dependency);
    }
  }
  return array($dependencies, $statuses);
}

/**
 * Alters the graph edge array, adding flags to indicate problematic
 * module installations.
 */
function _dependency_analysis_analyze(&$graph_edges, $module_statuses) {
  // track labelled edges in a copy (to keep graph analysis simpler)
  $graph_edges_labelled = $graph_edges;
  foreach ($graph_edges_labelled as $key => $value) {
    $graph_edges_labelled[$key] = array_combine($value, $value);
  }
  // apply graph analyzers
  $analyzers = array(
    '_dependency_analysis_analyze_missing_dependencies',
    '_dependency_analysis_analyze_subdependencies',
    '_dependency_analysis_analyze_circular_dependencies',
  );
  foreach ($analyzers as $analyzer) {
    $analyzer($graph_edges, $graph_edges_labelled, $module_statuses);
  }
  // return the labelled edges
  $graph_edges = $graph_edges_labelled;
}

/**
 * Generates DOT describing the graph.
 */
function _dependency_analysis_generate_dot($graph_edges, $module_statuses) {
  // header
  $site_name = variable_get('site_name', '');
  echo empty($site_name) ? "digraph {\n" : "digraph \"$site_name\" {\n";
  // loop over modules, generating an edge per dependency
  foreach ($graph_edges as $parent => $children) {
    // for disabled or uninstalled modules, just put a node
    if ($module_statuses[$parent] != DEPENDENCY_ANALYSIS_ENABLED) {
      $fillcolor = ($module_statuses[$parent] == DEPENDENCY_ANALYSIS_DISABLED) ? 'gray' : 'black" fontcolor="white';
      echo '"' . $parent . '" [style=filled fillcolor="' . $fillcolor . '"];' . "\n";
      continue;
    }
    // We escape the module names to avoid collisions
    // with DOT keywords such as "node" or "edge".
    $parent = '"' . $parent . '"';
    if (empty($children)) {
      // for childless parents, just put a node
      echo "$parent;\n";
    }
    else {
      // put an edge per child; if the edge carries misconfiguration data,
      // fill the parent node with the warning color, and color the edge
      $is_filled_node = array();
      foreach ($children as $child => $data) {
        $child = '"' . $child . '"';
        if (is_array($data)) {
          echo "$parent -> $child [color={$data[0]}];\n";
          $is_filled_node = array_unique(array_merge($is_filled_node, $data));
        }
        else {
          echo "$parent -> $child;\n";
        }
      }
      if (!empty($is_filled_node)) {
        echo "$parent [style=filled fillcolor=\"";
        $weight = 1.0 / count($is_filled_node);
        echo implode(";$weight:", $is_filled_node);
        echo "\"];\n";
      }
    }
  }
  // closing
  echo "}\n";
}

/**
 * Sanitize module names from system_rebuild_module_data().  It adds version
 * requirements to the module names, but we need need the names to be unique
 * the module required.
 */
function _dependency_analysis_sanitize_module_name($module_name) {
  $parts = explode(' ', $module_name);
  return $parts[0];
}

/**
 * Check to make sure that every module's dependencies are enabled.  This can
 * happen if a module was updated, the update added new dependencies, but there
 * was no update hook to enable the new dependencies.
 */
function _dependency_analysis_analyze_missing_dependencies($graph_edges, &$graph_edges_labelled, $module_statuses) {
  foreach ($graph_edges as $parent => $children) {
    foreach ($children as $child) {
      if (!isset($module_statuses[$child]) || $module_statuses[$child] != DEPENDENCY_ANALYSIS_ENABLED) {
        // mark as missing
        _dependency_analysis_add_flag($parent, $child, 'orange', $graph_edges_labelled);
      }
    }
  }
}

/**
 * Examine the graph for redundant subdependencies: if a module depends on
 * a module that is also a descendent of another child.
 */
function _dependency_analysis_analyze_subdependencies($graph_edges, &$graph_edges_labelled) {
  foreach ($graph_edges as $parent => $children) {
    foreach ($children as $child) {
      if (_dependency_analysis_is_descendant($child, array_diff($children, array($child)), $graph_edges)) {
        // mark as redundant
        _dependency_analysis_add_flag($parent, $child, 'yellow', $graph_edges_labelled);
      }
    }
  }
}

/**
 * Recursively check to see if $child is among $to_check or its descendants.
 */
function _dependency_analysis_is_descendant($child, $to_check, $descendants, $already_checked = array()) {
  if (empty($to_check)) {
    return FALSE;
  }
  elseif (in_array($child, $to_check)) {
    return TRUE;
  }
  else {
    // grab the children of the ones we just checked
    $new_to_check = array();
    foreach ($to_check as $new_child) {
      // add it to the list of modules we've checked (to avoid circular dependencies)
      $already_checked[] = $new_child;
      // if it's enabled, add its children (some installs are borked,
      // and we avoid checking the children of oddly missing modules)
      if (array_key_exists($new_child, $descendants)) {
        $new_to_check = array_merge($new_to_check, $descendants[$new_child]);
      }
    }
    // clean up duplicates, and remove any we've already checked
    $new_to_check = array_diff(array_unique($new_to_check), $already_checked);
    // recurse on the new children
    return _dependency_analysis_is_descendant($child, $new_to_check, $descendants, $already_checked);
  }
}

/**
 * Examine the graph for circular dependencies.
 */
function _dependency_analysis_analyze_circular_dependencies($graph_edges, &$graph_edges_labelled) {
  // There is a performance gain to be had by using the search already
  // performed by system_rebuild_module_data().  However, then tests
  // can't use a pre-computed list of modules and dependencies.
  $prefilter = FALSE;
  if ($prefilter) {
    // use the already-performed search to pre-filter modules that have themselves as dependencies
    foreach (system_rebuild_module_data() as $module_name => $module) {
      if (in_array($module_name, array_keys($module->requires))) { // requires or required_by
        // there must be a cycle; do a depth-first search starting here
        $graph_edges_copy = $graph_edges;
        _dependency_analysis_cycle_search($graph_edges_copy, $graph_edges_labelled, $module_name);
      }
    }
  }
  else {
    // do a depth-first search of each module's dependencies starting at the module
    foreach ($graph_edges as $module_name => $dependencies) {
      $graph_edges_copy = $graph_edges;
      _dependency_analysis_cycle_search($graph_edges_copy, $graph_edges_labelled, $module_name);
    }
  }
}

/**
 * Recursive depth-first search for all cycles.
 */
function _dependency_analysis_cycle_search(&$graph_edges, &$graph_edges_labelled, $module, $call_stack = array()) {
  $adjacent_edges = $graph_edges[$module];
  // check for a cycle among where we've been and where we're going
  $stack_intersects = array_intersect($adjacent_edges, $call_stack);
  if (!empty($stack_intersects)) {
    // label the back edges
    foreach ($stack_intersects as $child) {
      _dependency_analysis_add_flag($module, $child, 'red', $graph_edges_labelled);
    }
    // walk backwards on the stack, painting as we go
    // NB: there may have been a rho, not a pure cycle, and it may be multiply-connected
    $call_stack_copy = $call_stack;
    $child = $module;
    do {
      $parent = array_pop($call_stack_copy);
      _dependency_analysis_add_flag($parent, $child, 'red', $graph_edges_labelled);
      $child = $parent;
      $stack_intersects = array_diff($stack_intersects, array($child));
    } while (count($stack_intersects));
  }
  // remove $module's edges so we don't search it again
  unset($graph_edges[$module]);
  array_push($call_stack, $module);
  // loop over all unsearched adjacent modules
  foreach ($adjacent_edges as $child_module) {
    if (isset($graph_edges[$child_module])) {
      _dependency_analysis_cycle_search($graph_edges, $graph_edges_labelled, $child_module, $call_stack);
    }
  }
}

/**
 * Add graph metadata for a flagged edge.
 */
function _dependency_analysis_add_flag($parent, $child, $data, &$graph_edges_labelled) {
  if (is_array($graph_edges_labelled[$parent][$child])) {
    // this relationship is already tagged; add to it (if data not already present)
    if (!in_array($data, $graph_edges_labelled[$parent][$child])) {
      $graph_edges_labelled[$parent][$child][] = $data;
    }
  }
  else {
    // this relationship is not yet tagged
    $graph_edges_labelled[$parent][$child] = array($data);
  }
}
