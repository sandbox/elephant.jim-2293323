This module provides a drush command to generate and analyze the dependency graph of installed modules.

--- Background ---

Drupal modules often depend on other modules to do their work.  The relationships between modules, "x depends on y", can be represented as a directed graph.  The graph can be rendered visually, and inspected for problems.  Problems detected and highlighted in the graph produced by this module include: cyclic dependencies (red), dependency on a disabled/uninstalled module (orange), and redundant subdependencies (yellow).

The "mm-dot" command in D6 used to provide a dependency graph, but the mm module has been abandoned in favor of pm (which doesn't have the graphing capability). Also, mm didn't do any graph analysis to detect problematic dependencies. See https://www.drupal.org/project/drush_mm

--- Installation ---

You can install this module in a number of places since there is just the drush command.  You can install it in any location you store Drupal modules, in sites/all/drush, in your user's $HOME/.drush folder, or in the system-wide drush commands folder (for example, /usr/share/drush/commands).  You may need to clear the drush command cache before drush will recognize the new command, "drush cc drush".

The dependency graph generator produces text in the DOT language.  In order to render the graph, you will need to install graphviz or other graph visualization software such as canviz or Tulip. For example, on Debian or Ubuntu you can use "sudo apt-get install graphviz" to install graphviz. See http://graphviz.org/ and https://en.wikipedia.org/wiki/DOT_(graph_description_language)

If you want to use the included simpletest unit tests, create a dummy .info and .module file, and install in your modules directory.  The .info file should include "files[] = dependency_analysis.test".

--- Usage ---

To use, try "drush am | dot -Tpng -omodule.png".
